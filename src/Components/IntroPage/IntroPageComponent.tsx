import React from "react";
import { animated, useSpring } from "react-spring";
import "./IntroPageComponent.css";

export const IntroPageComponent: React.FC = () => {
    const IntroPageSloganConainerStyles = useSpring({
        delay: 800,
        from: {
            opacity: 0,
        },
        to: {
            // background: `rgba(149, 254, 147, 1)`,
            background: "linear-gradient(to right, #fe9394, #95fe93)",
            opacity: 1,
        },
    });

    return (
        <animated.div className="IntroPageComponent">
            <animated.div
                style={IntroPageSloganConainerStyles}
                className="IntroPageSloganConainer"
            >
                <h1>The healthcare app of the future</h1>
            </animated.div>
        </animated.div>
    );
};

export default IntroPageComponent;
