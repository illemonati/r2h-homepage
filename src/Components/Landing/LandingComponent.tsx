import React, { useCallback, useEffect, useState } from "react";
import "./LandingComponent.css";
import { useSpring, animated, useTransition } from "react-spring";
import { sleep } from "../../utilts";
import r2hLogo from "../../Assets/r2h_hires.png";
import mouseIcon from "../../Assets/mouse-icon.svg";

export const LandingComponent: React.FC = () => {
    const [letters, setLetters] = useState<{ index: number; val: string }[]>(
        []
    );
    const transitions = useTransition(
        letters,
        (item: { index: number; val: string }) => item.index,
        {
            //@ts-ignore
            config: { duration: 300 },
            from: { transform: "translate3d(0,-40px,0)", opacity: 0 },
            enter: { transform: "translate3d(0,0px,0)", opacity: 1 },
            leave: { transform: "translate3d(0,-40px,0)", opacity: 0 },
        }
    );

    // const landingComponentStyle = useSpring({
    //     config: { duration: 1000 },
    //     // config: { mass: 20 },
    //     from: {
    //         background: "linear-gradient(to right, #000000, #000000)",
    //         opacity: 0,
    //     },
    //     to: {
    //         // background: `rgba(149, 254, 147, 1)`,
    //         background: "linear-gradient(to right, #9395fe, #fe9394)",
    //         opacity: 1,
    //     },
    // });

    const landingComponentMouseScrollHintStyle = useSpring({
        config: { mass: 20 as any, clamp: true as any },
        delay: 1000,
        from: {
            transform: "translate(30px, -40px)",
            opacity: 0,
        },
        to: {
            transform: "translate(0px, 0px)",
            opacity: 1,
        },
    });

    const landingComponentR2HLogoStyle = useSpring({
        config: { duration: 2000 },
        // delay: 1000,
        from: {
            opacity: 0,
        },
        to: {
            opacity: 1,
        },
    });

    const enterLetters = useCallback(() => {
        let isCanceled = false;
        (async () => {
            const allLetters = [..."Resource 2 Health"];
            setLetters([]);
            for (const [i, letter] of allLetters.entries()) {
                const item = {
                    index: i,
                    val: letter === " " ? "\u00A0" : letter,
                };
                setLetters((letters) => [...letters, item]);
                await sleep(200);
                if (isCanceled) return;
            }
        })();
        return () => (isCanceled = true);
    }, []);
    useEffect(() => {
        const cleanUp = enterLetters();
        return () => {
            cleanUp();
        };
    }, [enterLetters]);
    return (
        <animated.div className="LandingComponent">
            <div className="LandingComponentR2HLogoContainer">
                <animated.img
                    className="LandingComponentR2HLogo"
                    style={landingComponentR2HLogoStyle}
                    src={r2hLogo}
                />
            </div>

            <animated.div className="LandingInformationSquare">
                {transitions.map(({ item, props, key }) => (
                    <animated.h1 key={key} style={props}>
                        {item.val}
                    </animated.h1>
                ))}
            </animated.div>

            <animated.div
                className="LandingComponentMouseScrollHint"
                style={landingComponentMouseScrollHintStyle}
            >
                <h2>Explore</h2>
                <img
                    src={mouseIcon}
                    alt=""
                    className="LandingComponentMouseScrollHintIcon"
                />
            </animated.div>
        </animated.div>
    );
};

export default LandingComponent;
