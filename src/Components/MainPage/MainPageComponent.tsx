import React, { useCallback, useEffect, useRef, useState } from "react";
import { CSSProperties } from "react";
import "./MainPageComponent.css";

import IntroPageComponent from "../IntroPage/IntroPageComponent";
import LandingComponent from "../Landing/LandingComponent";
import { useTransition, animated } from "react-spring";

const panels = [
    ({ style }: { style: CSSProperties }) => (
        <animated.div className="MainPagePanel" style={{ ...style }}>
            <LandingComponent />
        </animated.div>
    ),
    ({ style }: { style: CSSProperties }) => (
        <animated.div className="MainPagePanel" style={{ ...style }}>
            <IntroPageComponent />
        </animated.div>
    ),
];

const defaultPanelBackground = "linear-gradient(to right, #000000, #000000)";

const panelBackgrounds = [
    "linear-gradient(to right, #9395fe, #fe9394)",
    "linear-gradient(to right, #fe9394, #95fe93)",
];

const scrollTimeout = 2000;

export const MainPageComponent: React.FC = () => {
    const [panelIndex, setPanelIndex] = useState({ index: 0, lastIndex: -1 });

    const scrollOnTimeout = useRef(false);

    const handleScroll = useCallback((e) => {
        const delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));

        if (!scrollOnTimeout.current) {
            if (delta === -1) {
                console.log("up");
                setPanelIndex(({ index, lastIndex }) => ({
                    index: Math.min(index + 1, panels.length - 1),
                    lastIndex: index,
                }));
            }
            if (delta === 1) {
                console.log("down");
                setPanelIndex(({ index, lastIndex }) => ({
                    index: Math.min(index - 1, 0),
                    lastIndex: index,
                }));
            }
            scrollOnTimeout.current = true;
            setTimeout(() => {
                scrollOnTimeout.current = false;
            }, scrollTimeout);
        }

        e.preventDefault();
        return false;
    }, []);

    const panelTransitions = useTransition(panelIndex, (p) => p.index, {
        config: { duration: 500 },
        from: {
            opacity: 0,
        },
        enter: {
            opacity: 1,
        },
        leave: {
            opacity: 0,
        },
    });
    const backgroundTransitions = useTransition(panelIndex, (p) => p.index, {
        config: { duration: 500 },
        from: {
            background:
                panelBackgrounds[panelIndex.lastIndex] ||
                defaultPanelBackground,
        },
        enter: {
            background:
                panelBackgrounds[panelIndex.index] || defaultPanelBackground,
        },
        leave: {
            background:
                panelBackgrounds[panelIndex.lastIndex] ||
                defaultPanelBackground,
        },
    });

    useEffect(() => {
        window.addEventListener("mousewheel", handleScroll, {
            passive: false,
            capture: true,
        });
        return () => {
            window.removeEventListener("mousewheel", handleScroll);
        };
    }, [handleScroll]);

    return (
        <animated.div className="MainPageComponent">
            {panelTransitions.map(({ item, props, key }) => {
                const Panel = panels[item.index];
                return <Panel key={key} style={props} />;
            })}
            {backgroundTransitions.map(({ props, key }) => {
                return (
                    <animated.div
                        key={key}
                        style={props}
                        className="MainPageComponentBackgroundDiv"
                    ></animated.div>
                );
            })}
        </animated.div>
    );
};

export default MainPageComponent;
// panelTransitions.map(({ item, props, key }) => {
//     const Panel = panels[item.index];
//     return <Panel key={key} style={props} />;
// });
