import React from "react";
import MainPageComponent from "./Components/MainPage/MainPageComponent";
import "typeface-roboto";

const App: React.FC = () => {
    return (
        <div className="App">
            <MainPageComponent />
        </div>
    );
};

export default App;
